<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">


    <title>Start</title>

</head>
<body>
<form action="getTicket" method="GET">
    <input type="text" name="ticketId" value="Ticket ID">
    <input type="submit" value="view ticket"/>
</form>
<form action="getAllTickets" method="GET">
    <input type="hidden" name="allTickets" value="view all tickets">
    <input type="submit" value="view all tickets">
</form>
<form action="getTag" method="GET">
    <input type="text" name="tagId" value="Tag ID">
    <input type="submit" value="view tag"/>
</form>
<form action="getAllTags" method="GET">
    <input type="hidden" name="allTags" value="view all tags">
    <input type="submit" value="view all tags">
</form>

<form action="getProject" method="GET">
    <input type="text" name="ProjectId" value="Project ID">
    <input type="submit" value="view project"/>
</form>
<form action="getAllProjects" method="GET">
    <input type="hidden" name="action" value="show all Projects">
    <input type="submit" value="view all projects">
</form>
<form action="createProjectRequest" method="GET">
    <input type="text" name="newProjectName" value="new Project">
    <input type="submit" value="create new project">
</form>

<form action="getUser" method="GET">
    <input type="text" name="UserId" value="User ID">
    <input type="submit" value="view User"/>
</form>
<form action="getAllUsers" method="GET">
    <input type="hidden" name="action" value="show all Users">
    <input type="submit" value="view all users">
</form>
<form action="createUserRequest" method="GET">
    <input type="text" name="newUserName" value="new User">
    <input type="submit" value="create new user">
</form>
</body>
</html>
