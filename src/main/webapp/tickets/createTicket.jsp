<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>create Ticket</title>
</head>
<body>
<form action="createTicket" method="POST">
    <%
        String newTicketName = request.getParameter("ticketName");
        String forProject = request.getParameter("projectId");
    %>
    <div class="form-group">
        <label>Name</label>
        <input type="text" name="ticketName" value="<%=newTicketName%>">
    </div>
    <div class="form-group">
        <label>associated Project(ID)</label>
        <input type="number" name="associatedProject" value="<%=forProject%>">
    </div>
    <input type="submit" value="create">
</form>
<div><a href="../index.jsp">Home</a></div>
</body>
</html>
