<%@ page import="Model.Ticket" %>
<%@ page import="static com.sun.org.apache.xml.internal.security.keys.keyresolver.KeyResolver.length" %>
<%@ page import="java.util.List" %>
<%@ page import="Model.Project" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Banger
  Date: 30.06.2020
  Time: 14:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>show Tickets</title>
</head>
<body>
<h2>view Ticket</h2>
<%
    Ticket ticket = (Ticket) request.getAttribute("ticket");
    List<Ticket> tickets = (List<Ticket>) request.getAttribute("tickets");
%>
<table>
    <thead>
    <td>ID</td>
    <td>Name</td>
    <td>Project</td>
    <td>update</td>
    <td>delete</td>
    <td>Tags</td>
    </thead>
    <tbody>
    <%
        if (ticket != null || tickets != null) {
            if (ticket != null && tickets == null) {
                tickets = new ArrayList<Ticket>();
                tickets.add(ticket);
            }
            for (Ticket t : tickets) {
                int id = t.getId();
                String name = t.getName();
                String projectName = t.getProject().getName();

    %>
    <tr>
        <td><%=id%>
        </td>
        <td><%=name%>
        </td>
        <td><%=projectName%>
        </td>
        <td>
            <form action="updateTicketRequest" method="get">
                <input name="ticketId" type="hidden" value=<%=id%>>
                <input type="submit" value="update">
            </form>
        </td>
        <td>
            <form action="deleteTicketRequest" method="get">
                <input name="ticketId" type="hidden" value=<%=id%>>
                <input type="submit" value="delete">
            </form>
        </td>
        <td>
            <form action="modifyTagsRequest" method="get">
                <input name="ticketId" type="hidden" value=<%=id%>>
                <input type="submit" value="update Tags">
            </form>
        </td>
    </tr>
    <%
        }
    %>
    </tbody>
</table>
<%
} else {
%>
<%}%>
<div><a href="../index.jsp">Home</a></div>

</body>
</html>
