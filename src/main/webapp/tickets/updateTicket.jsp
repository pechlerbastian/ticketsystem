<%@ page import="Model.Ticket" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>update Ticket</title>
</head>
<body>
<div></div>
<form action="updateTicket" method="POST">
    <%  Ticket ticket =  (Ticket) request.getAttribute("ticket");
        System.out.println((ticket).getName());
        int id = ticket.getId();
    %>
    <input type="hidden" name="ticketId" value="<%=ticket.getId()%>">
    <div class="form-group">
        <label>Name</label>
        <input type="text" name="ticketName" value="<%=ticket.getName()%>">
    </div>
    <div class="form-group">
        <label>associated Project(ID)</label>
        <input type="number" name="associatedProject" value="<%=ticket.getProject().getId()%>">
    </div>
</form>
<form action="modifyTagsRequest" method="get">
    <input name="ticketId" type="hidden" value=<%=id%>>
    <input type="submit" value="update Tags">
</form>
<div><a href="../index.jsp">Home</a></div>
</body>
</html>