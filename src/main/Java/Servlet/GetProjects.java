package Servlet;

import DAO.ProjectDao;
import DAO.ProjectDaoImpl;
import Model.Project;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet({
        "/getProject",
        "/getAllProjects",
        "/createNewProject",
        "/createProjectRequest",
        "/updateProjectRequest",
        "/deleteProjectRequest",
        "/updateProject",
        "/deleteProject"
})


public class GetProjects extends HttpServlet {
    //ToDo filter: is user logged in? (as admin/ as user) -> change returned lists and objects according to accessibility

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String requestURI = request.getRequestURI();
        final int lastSlash = requestURI.lastIndexOf('/');
        ProjectDao projectDao = new ProjectDaoImpl();
        String ending = requestURI.substring(lastSlash);
        switch (ending) {
            //create new project
            case ("/createNewProject"): {
                String name = request.getParameter("projectName");
                Project create = new Project(name);
                create = projectDao.create(create);
                request.setAttribute("project", create);

                RequestDispatcher rd = request.getRequestDispatcher("projects/showProject.jsp");
                rd.forward(request, response);
            }//update project, set changed name if necessary, else return same project
            case ("/updateProject"): {
                int id = Integer.parseInt(request.getParameter("projectId"));
                Project update = projectDao.get(id);
                String oldName = update.getName();
                String newName = request.getParameter("projectName");
                if (!oldName.equals(newName)) {
                    update.setName(newName);
                    projectDao.update(update);
                }
                request.setAttribute("project", update);
                RequestDispatcher rd = request.getRequestDispatcher("projects/showProject.jsp");
                rd.forward(request, response);
            }//update project to deleted = true
            case ("/deleteProject"): {
                int id = Integer.parseInt(request.getParameter("projectId"));
                Project delete = projectDao.get(id);
                delete.setDeleted(true);
                projectDao.update(delete);

                List<Project> projects = projectDao.getAll();
                request.setAttribute("projects", projects);

                RequestDispatcher rd = request.getRequestDispatcher("projects/showProject.jsp");
                rd.forward(request, response);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String requestURI = request.getRequestURI();
        final int lastSlash = requestURI.lastIndexOf('/');
        ProjectDao projectDao = new ProjectDaoImpl();
        String ending = requestURI.substring(lastSlash);
        //get one specific project from id ToDo: also find by name (unique)
        switch (ending) {
            case ("/getProject"): {
                //missing: catch ID nicht vergeben, throw keine ID eingegeben
                int id = Integer.parseInt(request.getParameter("ProjectId"));
                Project read = projectDao.get(id);
                request.setAttribute("project", read);
                RequestDispatcher rd = request.getRequestDispatcher("projects/showProject.jsp");
                rd.forward(request, response);
            }//get all projects
            case ("/getAllProjects"): {
                List<Project> projects = projectDao.getAll();
                request.setAttribute("projects", projects);
                RequestDispatcher rd = request.getRequestDispatcher("projects/showProject.jsp");
                rd.forward(request, response);
            } //request redirects to createProject.jsp ToDo: redirect via javascript to keep parameter? maybe with modal
            case ("/createProjectRequest"): {
                RequestDispatcher rd = request.getRequestDispatcher("projects/createProject.jsp");
                rd.forward(request, response);
            }//request redirects to updateProject.jsp ToDo: redirect via javascript to keep parameter? maybe with modal (toUpdate should be loaded with javascript)
            case ("/updateProjectRequest"): {
                Project toUpdate = projectDao.get(Integer.parseInt(request.getParameter("projectId")));
                request.setAttribute("project", toUpdate);
                RequestDispatcher rd = request.getRequestDispatcher("projects/updateProject.jsp");
                rd.forward(request, response);
            }//request redirects to deleteProject.jsp ToDo: redirect via javascript to keep parameter? maybe with modal (toDelete should be loaded with javascript)
            case ("/deleteProjectRequest"): {
                Project toDelete = projectDao.get(Integer.parseInt(request.getParameter("projectId")));
                request.setAttribute("project", toDelete);
                RequestDispatcher rd = request.getRequestDispatcher("projects/deleteProject.jsp");
                rd.forward(request, response);
            }
        }
    }
}
