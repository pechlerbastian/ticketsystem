package Servlet;

import DAO.ProjectDaoImpl;
import DAO.TagDao;
import DAO.TagDaoImpl;
import DAO.TicketDao;
import DAO.TicketDaoImpl;
import Model.Project;
import Model.Tag;
import Model.Ticket;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet({
        "/createTag",
        "/updateTag",
        "/updateTagRequest",
        "/deleteTag",
        "/deleteTagRequest",
        "/getAllTags",
        "/getTag",
        "/getTagsForProject",
        "/addTagToProject",
        "/modifyTagsRequest",
        "/addTagToTicket",
        "/removeTagFromTicket"
})

public class GetTags extends HttpServlet {
    //ToDo filter: is user logged in? create, update, delete tags should be admin functions
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String requestURI = request.getRequestURI();
        final int lastSlash = requestURI.lastIndexOf('/');
        final String ending = requestURI.substring(lastSlash);
        TagDao tagDao = new TagDaoImpl();
        boolean condition1 = ending.equals("/addTagToTicket");
        //add/remove tags to/from tickets, after function go back to showTicket.jsp ToDo: go back to modify tags for ticket (jeavascript reload?)
        if (condition1 || ending.equals("/removeTagFromTicket")) {
            int tagId = Integer.parseInt(request.getParameter("tagId"));
            int ticketId = Integer.parseInt(request.getParameter("ticketId"));
            Tag tag = tagDao.get(tagId);
            TicketDao ticketDao = new TicketDaoImpl();
            Ticket ticket = ticketDao.get(ticketId);
            //because of FetchType.LAZY
            List<Tag> tags = tagDao.getAllForTicket(ticket);
            if (condition1) {
                tags.add(tag);
            } else {
                tags.remove(tag);
            }
            ticket.setTags(tags);
            ticketDao.update(ticket);
            String projectParam = request.getParameter("projectId");
            List<Ticket> tickets;
            //return right list of tickets, if accessed by project or all tickets are directly accessed
            if (projectParam != null) {
                ProjectDaoImpl projectDao = new ProjectDaoImpl();
                int projectId = Integer.parseInt(projectParam);
                tickets = ticketDao.getAllForProject(projectDao.get(projectId));
            } else {
                tickets = ticketDao.getAll();
            }
            request.setAttribute("tickets", tickets);
            RequestDispatcher rd = request.getRequestDispatcher("tickets/showTicket.jsp");
            rd.forward(request, response);
        }
        switch (ending) {
            //create new ticket
            case ("/createTag"): {
                String name = request.getParameter("tagName");
                int rank = Integer.parseInt(request.getParameter("tagRank"));
                ProjectDaoImpl projectDao = new ProjectDaoImpl();
                Project project = projectDao.get(Integer.parseInt(request.getParameter("associatedProject")));
                Tag create = new Tag();
                create.setName(name);
                create.setRank(rank);
                create.setProject(project);
                create = tagDao.create(create);
                request.setAttribute("tag", create);

                RequestDispatcher rd = request.getRequestDispatcher("tags/showTag.jsp");
                rd.forward(request, response);
            }//update tag, set changed name/rank/project if necessary, else return same tag
            case ("/updateTag"): {
                ProjectDaoImpl projectDao = new ProjectDaoImpl();
                int id = Integer.parseInt(request.getParameter("tagId"));
                Tag update = tagDao.get(id);
                int projectIdNew = Integer.parseInt(request.getParameter("associatedProject"));
                int projectIdOld = update.getProject().getId();
                String nameNew = request.getParameter("tagName");
                String nameOld = update.getName();
                int rankOld = update.getRank();
                int rankNew = Integer.parseInt(request.getParameter("tagRank"));
                boolean con1 = projectIdNew != projectIdOld;
                boolean con2 = !nameNew.equals(nameOld);
                boolean con3 = rankOld != rankNew;
                if (con1 || con2 || con3) {
                    if (con1) {
                        Project project = projectDao.get(projectIdNew);
                        update.setProject(project);
                    }
                    if (con2) {
                        update.setName(nameNew);
                    }
                    if (con3)
                        tagDao.update(update);
                }
                request.setAttribute("tag", update);
                RequestDispatcher rd = request.getRequestDispatcher("tags/showTag.jsp");
                rd.forward(request, response);

            }//update tag, set deleted=true
            case ("/deleteTag"): {
                int id = Integer.parseInt(request.getParameter("tagId"));
                Tag delete = tagDao.get(id);
                delete.setDeleted(true);
                tagDao.update(delete);
                List<Tag> tags = tagDao.getAll();
                request.setAttribute("tags", tags);
                RequestDispatcher rd = request.getRequestDispatcher("tags/showTag.jsp");
                rd.forward(request, response);
            }

        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String requestURI = request.getRequestURI();
        final int lastSlash = requestURI.lastIndexOf('/');
        final String ending = requestURI.substring(lastSlash);
        TagDaoImpl tagDao = new TagDaoImpl();
        switch (ending) {
            //get one specific tag from id ToDo: also find by name (unique)
            case ("/getTag"): {
                int id = Integer.parseInt(request.getParameter("tagId"));
                Tag tag = tagDao.get(id);
                request.setAttribute("tag", tag);
                RequestDispatcher rd = request.getRequestDispatcher("tags/showTag.jsp");
                rd.forward(request, response);
            }//get all tags
            case ("/getAllTags"): {
                List<Tag> tags = tagDao.getAll();
                request.setAttribute("tags", tags);
                RequestDispatcher rd = request.getRequestDispatcher("tags/showTag.jsp");
                rd.forward(request, response);
            }//get all tags for specific project
            case ("/getTagsForProject"): {
                ProjectDaoImpl projectDao = new ProjectDaoImpl();
                Project project = projectDao.get(Integer.parseInt(request.getParameter("projectId")));
                List<Tag> tags = tagDao.getAllForProject(project);
                request.setAttribute("tags", tags);
                RequestDispatcher rd = request.getRequestDispatcher("tags/showTag.jsp");
                rd.forward(request, response);
            }//add a new tag, create it within a project (create request, forward to createTag.jsp)
            case ("/addTagToProject"): {
                request.getRequestDispatcher("tags/createTag.jsp").forward(request, response);
            }//request redirects to deleteTag.jsp ToDo: redirect via javascript to keep parameter? maybe with modal (toUpdate should be loaded with javascript)
            case ("/updateTagRequest"): {
                Tag toUpdate = tagDao.get(Integer.parseInt(request.getParameter("tagId")));
                request.setAttribute("tag", toUpdate);
                RequestDispatcher rd = request.getRequestDispatcher("tags/updateTag.jsp");
                rd.forward(request, response);
            }//request redirects to deleteProject.jsp ToDo: redirect via javascript to keep parameter? maybe with modal (toDelete should be loaded with javascript)
            case("/deleteTagRequest"): {
                Tag toDelete = tagDao.get(Integer.parseInt(request.getParameter("tagId")));
                request.setAttribute("tag", toDelete);
                RequestDispatcher rd = request.getRequestDispatcher("tags/deleteTag.jsp");
                rd.forward(request, response);
            }//list1: all tags which are already assigned to a ticket, list2: all tags which are in tickets project (not assigned)
            case ("/modifyTagsRequest"): {
                TicketDaoImpl ticketDao = new TicketDaoImpl();
                Ticket ticket = ticketDao.get(Integer.parseInt(request.getParameter("ticketId")));
                ProjectDaoImpl projectDao = new ProjectDaoImpl();
                Project project = projectDao.get(ticket.getProject().getId());
                List<Tag> tagsForTicket = tagDao.getAllForTicket(ticket);
                List<Tag> tagsForProject = tagDao.getAllForProject(project);
                tagsForProject.removeAll(tagsForTicket);
                request.setAttribute("tags", tagsForTicket);
                request.setAttribute("tagsCanBeAdded", tagsForProject);
                RequestDispatcher rd = request.getRequestDispatcher("tags/showTag.jsp");
                rd.forward(request, response);
            }
        }
    }
}
