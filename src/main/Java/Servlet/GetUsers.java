package Servlet;

import DAO.ProjectDao;
import DAO.ProjectDaoImpl;
import DAO.UserDao;
import DAO.UserDaoImpl;
import Model.Project;
import Model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet({
        "/getUser",
        "/getAllUsers",
        "/createUserRequest",
        "/updateUserRequest",
        "/deleteUserRequest",
        "/updateUser",
        "/createUser",
        "/deleteUser",
        "/modifyUsersInProject",
        "/addUserToProject",
        "/removeUserFromProject"
})


public class GetUsers extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String requestURI = request.getRequestURI();
        final int lastSlash = requestURI.lastIndexOf('/');
        UserDao userDao = new UserDaoImpl();
        String ending = requestURI.substring(lastSlash);
        boolean condition1 = ending.equals("/addUserToProject");
        //add/remove tags to/from tickets, after function go back to showTicket.jsp ToDo: go back to modify tags for ticket (jeavascript reload?)
        if (condition1 || ending.equals("/removeUserFromProject")) {
            int userId = Integer.parseInt(request.getParameter("userId"));
            int projectId = Integer.parseInt(request.getParameter("projectId"));
            User user = userDao.get(userId);
            ProjectDao projectDao = new ProjectDaoImpl();
            Project project = projectDao.get(projectId);
            //because of FetchType.LAZY
            List<User> users = userDao.getAllForProject(project);
            //check if user should be added or removed, update user list in project (due to fetchtype.lazy)
            if (condition1) {
                users.add(user);
            } else {
                users.remove(user);
            }
            project.setUsers(users);
            projectDao.update(project);
            List<Project> projects = projectDao.getAll();
            request.setAttribute("projects", projects);
            RequestDispatcher rd = request.getRequestDispatcher("projects/showProject.jsp");
            rd.forward(request, response);
        }
        switch (ending) {
            //create new user
            case ("/createUser"): {
                String name = request.getParameter("userName");
                String password = request.getParameter("userPassword");
                boolean isAdmin = (request.getParameter("userAdmin") != null);
                User create = new User();
                create.setName(name);
                create.setPassword(password);
                create.setAdmin(isAdmin);
                create = userDao.create(create);
                request.setAttribute("user", create);

                RequestDispatcher rd = request.getRequestDispatcher("users/showUser.jsp");
                rd.forward(request, response);
            }//update user, set changed name, password, admin state if necessary, else return same user
            case ("/updateUser"): {
                int id = Integer.parseInt(request.getParameter("userId"));
                User update = userDao.get(id);
                String nameNew = request.getParameter("userName");
                String nameOld = update.getName();
                String passwordNew = request.getParameter("userPassword");
                String passwordOld = update.getPassword();
                boolean adminNew = (request.getParameter("userAdmin") != null);
                boolean adminOld = update.isAdmin();
                if (adminNew != adminOld || !nameNew.equals(nameOld) || !passwordNew.equals(passwordOld)) {
                    if (adminNew != adminOld) {
                        update.setAdmin(adminNew);
                    }
                    if (!nameNew.equals(nameOld)) {
                        update.setName(nameNew);
                    }
                    if(!passwordNew.equals(passwordOld)){
                        update.setPassword(passwordNew);
                    }
                    userDao.update(update);
                }
                request.setAttribute("user", update);
                RequestDispatcher rd = request.getRequestDispatcher("users/showUser.jsp");
                rd.forward(request, response);
            }//update user to deleted=true
            case ("/deleteUser"): {
                int id = Integer.parseInt(request.getParameter("userId"));
                User toDelete = userDao.get(id);
                toDelete.setDeleted(true);
                userDao.update(toDelete);

                List<User> users = userDao.getAll();
                request.setAttribute("users", users);

                RequestDispatcher rd = request.getRequestDispatcher("users/showUser.jsp");
                rd.forward(request, response);
            }//add user to a specific project
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String requestURI = request.getRequestURI();
        final int lastSlash = requestURI.lastIndexOf('/');
        UserDao userDao = new UserDaoImpl();
        String ending = requestURI.substring(lastSlash);
        switch (ending) {
            //get one specific user from id ToDo: also find by name (unique)
            case ("/getUser"): {
                User user = userDao.get(Integer.parseInt(request.getParameter("userId")));
                request.setAttribute("user", user);
                RequestDispatcher rd = request.getRequestDispatcher("users/showUser.jsp");
                rd.forward(request, response);
            }//get all users
            case ("/getAllUsers"): {
                List<User> users = userDao.getAll();
                request.setAttribute("users", users);

                RequestDispatcher rd = request.getRequestDispatcher("users/showUser.jsp");
                rd.forward(request, response);
            }//request redirects to createUser.jsp ToDo: redirect via javascript to keep parameter? maybe with modal
            case ("/createUserRequest"): {
                RequestDispatcher rd = request.getRequestDispatcher("users/createUser.jsp");
                rd.forward(request, response);
            }//request redirects to updateUser.jsp ToDo: redirect via javascript to keep parameter? maybe with modal (toUpdate should be loaded with javascript)
            case ("/updateUserRequest"): {
                User toUpdate = userDao.get(Integer.parseInt(request.getParameter("userId")));
                request.setAttribute("user", toUpdate);
                RequestDispatcher rd = request.getRequestDispatcher("users/updateUser.jsp");
                rd.forward(request, response);
            }//request redirects to deleteTicket.jsp ToDo: redirect via javascript to keep parameter? maybe with modal (toDelete should be loaded with javascript)
            case ("/deleteUserRequest"): {
                User toDelete = userDao.get(Integer.parseInt(request.getParameter("userId")));
                request.setAttribute("user", toDelete);
                RequestDispatcher rd = request.getRequestDispatcher("users/deleteUser.jsp");
                rd.forward(request, response);
            }//request to see all users in project (list1) and users who can be added
            case ("/modifyUsersInProject"): {
                ProjectDao projectDao = new ProjectDaoImpl();
                Project project = projectDao.get(Integer.parseInt(request.getParameter("projectId")));
                List<User> userIn = userDao.getAllForProject(project);
                List<User> canBeAssigned = userDao.getAll();
                canBeAssigned.removeAll(userIn);
                request.setAttribute("users", userIn);
                request.setAttribute("usersAssignable", canBeAssigned);
                RequestDispatcher rd = request.getRequestDispatcher("users/showUser.jsp");
                rd.forward(request, response);

            }
        }
    }
}
