package Servlet;

import DAO.ProjectDaoImpl;
import DAO.TagDaoImpl;
import DAO.TicketDaoImpl;
import Model.Project;
import Model.Tag;
import Model.Ticket;

import javax.servlet.annotation.WebServlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet({
        "/getTicket",
        "/getAllTickets",
        "/createTicket",
        "/createTicketRequest",
        "/updateTicketRequest",
        "/deleteTicketRequest",
        "/updateTicket",
        "/deleteTicket",
        "/getTicketsForProject",
        "/addTicketToProject",
        "/getTicketsWithTag"
})

public class GetTickets extends HttpServlet {
    //ToDo filter: is user logged in?

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String requestURI = request.getRequestURI();
        final int lastSlash = requestURI.lastIndexOf('/');
        TicketDaoImpl dao = new TicketDaoImpl();
        ProjectDaoImpl projectDao = new ProjectDaoImpl();
        final String ending = requestURI.substring(lastSlash);
        //create new ticket: with data from jsp
        switch (ending) {
            //create new ticket
            case ("/createTicket"): {
                String name = request.getParameter("ticketName");
                int projectId = Integer.parseInt(request.getParameter("associatedProject"));
                Ticket create = new Ticket(name, projectDao.get(projectId));
                create = dao.create(create);
                request.setAttribute("ticket", create);

                RequestDispatcher rd = request.getRequestDispatcher("tickets/showTicket.jsp");
                rd.forward(request, response);
            }//update ticket, if name or project changed
            case ("/updateTicket"): {
                int id = Integer.parseInt(request.getParameter("ticketId"));
                Ticket update = dao.get(id);
                int projectIdNew = Integer.parseInt(request.getParameter("associatedProject"));
                int projectIdOld = update.getProject().getId();
                String nameNew = request.getParameter("ticketName");
                String nameOld = update.getName();
                if (projectIdNew != projectIdOld || !nameNew.equals(nameOld)) {
                    if (projectIdNew != projectIdOld) {
                        Project project = projectDao.get(projectIdNew);
                        update.setProject(project);
                    }
                    if (!nameNew.equals(nameOld)) {
                        update.setName(nameNew);
                    }
                    dao.update(update);
                }
                request.setAttribute("ticket", update);
                RequestDispatcher rd = request.getRequestDispatcher("tickets/showTicket.jsp");
                rd.forward(request, response);
            }//update ticket to deleted = true
            case ("/deleteTicket"): {
                int id = Integer.parseInt(request.getParameter("ticketId"));
                Ticket delete = dao.get(id);
                delete.setDeleted(true);
                dao.update(delete);

                List<Ticket> tickets = dao.getAll();
                request.setAttribute("tickets", tickets);

                RequestDispatcher rd = request.getRequestDispatcher("tickets/showTicket.jsp");
                rd.forward(request, response);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String requestURI = request.getRequestURI();
        final int lastSlash = requestURI.lastIndexOf('/');
        TicketDaoImpl ticketDao = new TicketDaoImpl();
        final String ending = requestURI.substring(lastSlash);
        //get one specific ticket from id ToDo: also find by name (unique)
        switch (ending) {
            case ("/getTicket"): {
                int id = Integer.parseInt(request.getParameter("ticketId"));
                Ticket t1 = ticketDao.get(id);
                request.setAttribute("ticket", t1);
                RequestDispatcher rd = request.getRequestDispatcher("tickets/showTicket.jsp");
                rd.forward(request, response);
            }//get all tickets
            case ("/getAllTickets"): {
                List<Ticket> tickets = ticketDao.getAll();
                request.setAttribute("tickets", tickets);

                RequestDispatcher rd = request.getRequestDispatcher("tickets/showTicket.jsp");
                rd.forward(request, response);
            }//request redirects to updateTicket.jsp ToDo: redirect via javascript to keep parameter? maybe with modal (toUpdate should be loaded with javascript)
            case ("/updateTicketRequest"): {
                Ticket toUpdate = ticketDao.get(Integer.parseInt(request.getParameter("ticketId")));
                request.setAttribute("ticket", toUpdate);
                RequestDispatcher rd = request.getRequestDispatcher("tickets/updateTicket.jsp");
                rd.forward(request, response);
            }//request redirects to deleteTicket.jsp ToDo: redirect via javascript to keep parameter? maybe with modal (toDelete should be loaded with javascript)
            case ("/deleteTicketRequest"): {
                Ticket toDelete = ticketDao.get(Integer.parseInt(request.getParameter("ticketId")));
                request.setAttribute("ticket", toDelete);
                RequestDispatcher rd = request.getRequestDispatcher("tickets/deleteTicket.jsp");
                rd.forward(request, response);
            }//get all tickets for specific project
            case ("/getTicketsForProject"): {
                int projectId = Integer.parseInt(request.getParameter("projectId"));
                ProjectDaoImpl projectDao = new ProjectDaoImpl();
                Project project = projectDao.get(projectId);
                List<Ticket> tickets = ticketDao.getAllForProject(project);
                request.setAttribute("tickets", tickets);
                RequestDispatcher rd = request.getRequestDispatcher("tickets/showTicket.jsp");
                rd.forward(request, response);
            }//get all tickets with specific tag
            case ("/getTicketsWithTag"): {
                int tagId = Integer.parseInt(request.getParameter("tagId"));
                TagDaoImpl tagDao = new TagDaoImpl();
                Tag tag = tagDao.get(tagId);
                List<Ticket> tickets = ticketDao.getTicketWithTag(tag);
                request.setAttribute("tickets", tickets);
                RequestDispatcher rd = request.getRequestDispatcher("tickets/showTicket.jsp");
                rd.forward(request, response);
            }//add a new ticket, create it within a project (create request, forward to createTicket.jsp)
            case ("/addTicketToProject"): {
                request.getRequestDispatcher("tickets/createTicket.jsp").forward(request, response);
            }
        }
    }
}
