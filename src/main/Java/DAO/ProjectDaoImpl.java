package DAO;

import Model.Project;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class ProjectDaoImpl implements ProjectDao{
    private static SessionFactory factory;
    static{
        try {
            factory = new Configuration().configure().buildSessionFactory();
        }catch(Throwable exc){
            System.err.println("Failed to create session object" + exc);
            throw new ExceptionInInitializerError(exc);
        }
    }

    public Project get(int id){
        Session session = factory.openSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            Project project = session.createQuery("from Project where id = :id", Project.class)
                    .setParameter("id", id).uniqueResult();
            transaction.commit();
            return project;
        }catch (HibernateException exception){
            if(transaction != null) transaction.rollback();
            exception.printStackTrace();
            return null;
        }finally {
            session.close();
        }
    }

    public List<Project> getAll(){
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            List<Project> projects = session.createQuery("from Project where deleted = false order by id", Project.class)
                    .getResultList();
            transaction.commit();
            return projects;
        }catch(HibernateException exception){
            if(transaction != null) transaction.rollback();
            exception.printStackTrace();
            return null;
        }finally {
            session.close();
        }
    }

    @Override
    public List<Project> getAllForAdmin() {
        return null;
    }

    public Project create(Project project){
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(project);
            session.getTransaction().commit();
        }catch(HibernateException exception){
            if(transaction != null)
                transaction.rollback();
            exception.printStackTrace();
            return null;
        }finally {
            session.close();
            return project;
        }

    }
    public Project update(Project project){
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(project);
            session.getTransaction().commit();
        }catch(HibernateException exception){
            if(transaction != null)
                transaction.rollback();
            exception.printStackTrace();
            return null;
        }finally {
            session.close();
            return project;
        }
    }
}
