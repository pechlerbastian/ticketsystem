package DAO;

import Model.Project;
import Model.User;

import java.util.List;

public interface UserDao {
    /**
     * Get a user from the database given its unique id.
     * @param id unique identifier of the user
     * @return the project from the database
     */
    User get(int id);

    /**
     * Get a user from the database given its unique id.
     * @param name unique identifier of the user
     * @return the project from the database
     */
    User get(String name);

    /**
     * Get all users from database where deleted = false (ToDo: which can be accessed by User)
     * @return list of all users
     */
    List<User> getAll();

    /**
     * Get all users from database where deleted = false which are assigned to a project(ToDo: which can be accessed by User)
     * @param project to find all assigned users for
     * @return list of all users
     */
    public List<User> getAllForProject(Project project);

    /**
     * create a given user in the database
     * id for this user is created, if project is saved successfully it is returned
     * @param user to be persisted in the database (w/o id)
     * @return persisted user
     */
    User create(User user);

    /**
     * update a given user in the database
     * user in database has other attributes than the user (parameter), new attributes are saved in database
     * @param user to be persisted in the database
     * @return persisted user
     */
    User update(User user);
}
