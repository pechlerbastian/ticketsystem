package DAO;

import Model.Project;
import Model.Tag;
import Model.Ticket;

import java.util.List;

public interface TicketDao {

    //ToDo session creation in utility class, get, getAll, create, update in more abstract class to be implemented for all Dao classes, create update in one methode?
    /**
     * Get a ticket from the database given its unique id.
     * @param id unique identifier of the ticket
     * @return the tag from the database
     */
    public Ticket get(int id);

    /**
     * Get all tickets from database where deleted = false (ToDo: probably should be admin feature, maybe w/o deleted=false)
     * @return list of all tickets
     */
    public List<Ticket> getAll();


    /**
     * Get all tickets from database where deleted = false which are marked with a specific tag
     * needed to display tickets with specific tag
     * (ToDo: which can be accessed by User)
     * @param tag which is assigned to every ticket in result list
     * @return list of all tickets with tag
     */
    public List<Ticket> getTicketWithTag(Tag tag);

    /**
     * Get all tickets of a specific project from database where deleted = false
     * (ToDo: which can be accessed by User)
     * @param project for which all tickets shall be displayed
     * @return list of all tickets created in a project
     */
    public List<Ticket> getAllForProject(Project project);


    /**
     * create a given ticket in the database
     * id for this ticket is created, if project is saved successfully it is returned
     * @param ticket to be persisted in the database (w/o id)
     * @return persisted ticket
     */
    public Ticket create(Ticket ticket);

    /**
     * update a given ticket in the database
     * ticket in database has other attributes than the ticket (parameter), new attributes are saved in database
     * @param ticket to be persisted in the database
     * @return persisted ticket
     */
    public Ticket update(Ticket ticket);
}
