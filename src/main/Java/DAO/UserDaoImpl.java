package DAO;

import Model.Project;
import Model.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class UserDaoImpl implements UserDao {

    private static SessionFactory factory;

    static {
        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable exc) {
            System.err.println("Failed to create session object" + exc);
            throw new ExceptionInInitializerError(exc);
        }
    }

    @Override
    public User get(int id) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            User user = session.createQuery("from User where id = :id", User.class)
                    .setParameter("id", id).uniqueResult();
            transaction.commit();
            return user;
        } catch (HibernateException exception) {
            if (transaction != null) transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    @Override
    public User get(String name) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            User user = session.createQuery("from User where name like :name", User.class)
                    .setParameter("name", name).uniqueResult();
            transaction.commit();
            return user;
        } catch (HibernateException exception) {
            if (transaction != null) transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    @Override
    public List<User> getAll() {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            List<User> users = session.createQuery("from User where deleted = false", User.class)
                    .getResultList();
            transaction.commit();
            return users;
        } catch (HibernateException exception) {
            if (transaction != null) transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    @Override
    public List<User> getAllForProject(Project project) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            List<User> users = session.createQuery("select u from User u join u.projects pu where pu = :project and u.deleted = false", User.class)
                    .setParameter("project", project).getResultList();
            transaction.commit();
            return users;
        } catch (HibernateException exception) {
            if (transaction != null) transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    @Override
    public User create(User user) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            if (transaction != null)
                transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
            return user;
        }
    }

    @Override
    public User update(User user) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            if (transaction != null)
                transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
            return user;
        }
    }
}
