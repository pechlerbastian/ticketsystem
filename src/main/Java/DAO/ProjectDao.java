package DAO;

import Model.Project;

import java.util.List;

public interface ProjectDao {

    //ToDo session creation in utility class, get, getAll, create, update in more abstract class to be implemented for all Dao classes, create update in one methode?

    /**
     * Get a project from the database given its unique id.
     * @param id unique identifier of the project
     * @return the project from the database
     */
    Project get(int id);

    /**
     * Get all projects from database where deleted = false (ToDo: which can be accessed by User)
     * @return list of all projects
     */
    List<Project> getAll();

    /**
     * Get all projects from database where deleted = false (ToDo: all with deleted?)
     * @return list of all projects
     */
    List<Project> getAllForAdmin();

    /**
     * create a given project in the database
     * id for this project is created, if project is saved successfully it is returned
     * @param project to be persisted in the database (w/o id)
     * @return persisted project
     */
    Project create(Project project);

    /**
     * update a given project in the database
     * project in database has other attributes than the project (parameter), new attributes are saved in database
     * @param project to be persisted in the database
     * @return persisted project
     */
    Project update(Project project);

}
