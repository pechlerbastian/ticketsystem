package DAO;

import Model.Project;
import Model.Tag;
import Model.Ticket;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class TicketDaoImpl implements TicketDao{
    private static SessionFactory factory;

    static {
        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable exc) {
            System.err.println("Failed to create session object" + exc);
            throw new ExceptionInInitializerError(exc);
        }
    }

    public Ticket get(int id) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Ticket ticket = session.createQuery("from Ticket where id = :id", Ticket.class)
                    .setParameter("id", id).uniqueResult();
            transaction.commit();
            return ticket;
        } catch (HibernateException exception) {
            if (transaction != null) transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public List<Ticket> getAll() {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            List<Ticket> tickets = session.createQuery("from Ticket where deleted = false order by id", Ticket.class)
                    .getResultList();
            transaction.commit();
            return tickets;
        } catch (HibernateException exception) {
            if (transaction != null) transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public List<Ticket> getAllForProject(Project project) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            String query = "select t from Ticket t, Project p where p = :project and p.id = t.project.id order by t.id";
            List<Ticket> tickets = session.createQuery(query, Ticket.class)
                    .setParameter("project", project).getResultList();
            transaction.commit();
            return tickets;
        } catch (HibernateException exception) {
            if (transaction != null) transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }


    public List<Ticket> getTicketWithTag(Tag tag) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            String query = "select t from Ticket t join t.tags ta where ta = :tag and t.deleted = false";
            List<Ticket> tickets = session.createQuery(query, Ticket.class)
                    .setParameter("tag", tag).getResultList();
            transaction.commit();
            return tickets;
        } catch (HibernateException exception) {
            if (transaction != null) transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public Ticket create(Ticket ticket) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(ticket);
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            if (transaction != null)
                transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
            return ticket;
        }

    }

    public Ticket update(Ticket ticket) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(ticket);
            session.getTransaction().commit();
        } catch (HibernateException exception) {
            if (transaction != null)
                transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
            return ticket;
        }
    }

}
