package DAO;

import Model.Project;
import Model.Tag;
import Model.Ticket;

import java.util.List;

public interface TagDao {

    //ToDo session creation in utility class, get, getAll, create, update in more abstract class to be implemented for all Dao classes, create update in one methode?
    /**
     * Get a tag from the database given its unique id.
     * @param id unique identifier of the tag
     * @return the tag from the database
     */
    public Tag get(int id);

    /**
     * Get all tags from database where deleted = false (ToDo: probably should be admin feature, maybe w/o deleted=false)
     * @return list of all tags
     */
    public List<Tag> getAll();

    /**
     * Get all tags from database where deleted = false which are assigned to a ticket
     * needed to update tags given to ticket
     * (ToDo: which can be accessed by User)
     * @param ticket for which the tags shall be displayed
     * @return list of all tags assigned to a ticket
     */
    public List<Tag> getAllForTicket(Ticket ticket);

    /**
     * Get all tags of a specific project from database where deleted = false
     * (ToDo: which can be accessed by User)
     * needed to see which tags can be added to a ticket belonging to a project
     * @param project for which all tags shall be displayed
     * @return list of all tags created in a project
     */
    public List<Tag> getAllForProject(Project project);


    /**
     * create a given project in the database
     * id for this tag is created, if tag is saved successfully it is returned
     * @param tag to be persisted in the database (w/o id)
     * @return persisted tag
     */
    public Tag create(Tag tag);

    /**
     * update a given tag in the database
     * tag in database has other attributes than the tag (parameter), new attributes are saved in database
     * @param tag to be persisted in the database
     * @return persisted tag
     */
    public Tag update(Tag tag);
}
