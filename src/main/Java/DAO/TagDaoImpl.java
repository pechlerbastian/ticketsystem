package DAO;

import Model.Project;
import Model.Tag;
import Model.Ticket;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class TagDaoImpl implements TagDao{
    private static SessionFactory factory;

    static {
        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable exc) {
            System.err.println("Failed to create session object" + exc);
            throw new ExceptionInInitializerError(exc);
        }
    }

    public Tag get(int id) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Tag tag = session.createQuery("from Tag where id = :id", Tag.class)
                    .setParameter("id", id).uniqueResult();
            transaction.commit();
            return tag;
        } catch (HibernateException exception) {
            if (transaction != null) transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public List<Tag> getAll() {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            List<Tag> tags = session.createQuery("from Tag where deleted=false", Tag.class)
                    .getResultList();
            transaction.commit();
            return tags;
        } catch (HibernateException exception) {
            if (transaction != null)
                transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public List<Tag> getAllForProject(Project project) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            String query = "select t from Tag t, Project p where p = :project and p.id = t.project.id order by t.id";
            List<Tag> tags = session.createQuery(query, Tag.class)
                    .setParameter("project", project).getResultList();
            transaction.commit();
            return tags;
        } catch (HibernateException exception) {
            if (transaction != null) transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public List<Tag> getAllForTicket(Ticket ticket) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            String query = "select t from Tag t join t.tickets ti where ti = :ticket and t.deleted = false";
            List<Tag> tags = session.createQuery(query, Tag.class)
                    .setParameter("ticket", ticket).getResultList();
            transaction.commit();
            return tags;
        } catch (HibernateException exception) {
            if (transaction != null) transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public Tag create(Tag tag) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(tag);
            transaction.commit();
            return tag;
        } catch (HibernateException exception) {
            if (transaction != null)
                transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public Tag update(Tag tag) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(tag);
            transaction.commit();
            return tag;
        } catch (HibernateException exception) {
            if (transaction != null)
                transaction.rollback();
            exception.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

}
