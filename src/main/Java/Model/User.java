package Model;

import lombok.*;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@DynamicInsert
@Entity
@Table(name="`user`")
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column
    private Integer id;

    @NonNull
    @Column
    private String name;

    @NonNull
    @Column
    private String password;

    @ManyToMany(mappedBy = "users", fetch=FetchType.LAZY)
    List<Project> projects;

    @NonNull
    @Column
    private boolean admin;

    @NonNull
    @Column
    private boolean deleted = false;

    //necessary for removeAll()
    @Override
    public boolean equals(Object object){
        if(!(object instanceof User))
            return false;
        User user = (User) object;
        return user.getId().equals(getId());
    }

}
