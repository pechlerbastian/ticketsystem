package Model;

import lombok.*;
import org.hibernate.annotations.DynamicInsert;

import java.util.List;
import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@DynamicInsert
@Entity
@Table(name = "ticket")
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;

    @NonNull
    @Column
    private String name;

    @NonNull
    @ManyToOne
    @JoinColumn(name = "project")
    private Project project;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(
            name = "ticket_tag",
            joinColumns = {@JoinColumn(name = "ticket")},
            inverseJoinColumns = {@JoinColumn(name = "tag")}
    )
    private List<Tag> tags;

    @NonNull
    @Column
    private boolean deleted = false;

}
