package Model;

import lombok.*;
import org.hibernate.annotations.DynamicInsert;

import java.util.List;
import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@DynamicInsert
@Entity
@Table(name="tag")
public class Tag {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column
    private Integer id;

    @NonNull
    @Column
    private String name;

    //necessary for ordering different tags by importance (e.g. immediately > test > review...)
    @Column
    @NonNull
    private Integer rank;

    @NonNull
    @ManyToOne
    @JoinColumn(name = "project")
    private Project project;

    //ToDo change to List
    @ManyToMany(mappedBy = "tags", fetch=FetchType.LAZY)
    List<Ticket> tickets;

    @NonNull
    @Column
    private boolean deleted = false;

    //necessary for removeAll()
    @Override
    public boolean equals(Object object){
        if(!(object instanceof Tag))
            return false;
        Tag tag = (Tag) object;
        return tag.getId().equals(getId());
    }
}
