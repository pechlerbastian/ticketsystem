package Model;

import lombok.*;
import org.hibernate.annotations.DynamicInsert;

import java.util.List;
import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@DynamicInsert
@Entity
@Table(name="project")
public class Project {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column
    private Integer id;

    @NonNull
    @Column
    private String name;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(
            name = "user_project",
            joinColumns = {@JoinColumn(name = "project")},
            inverseJoinColumns = {@JoinColumn(name = "`user`")}
    )
    private List<User> users;

    @NonNull
    @Column
    private boolean deleted = false;

}
