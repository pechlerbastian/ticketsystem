<%@ page import="Model.Tag" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>delete Tag</title>
</head>
<body>
<form action="deleteTag" method="POST">
    <%Tag tag = (Tag) request.getAttribute("tag");%>
    <div class="form-group">
        <label>tag ID</label>
        <input type="hidden" name="tagId" value="<%=tag.getId()%>">
    </div>
    <div class="form-group">
        <label>Name</label>
        <input type="text" name="tagName" value="<%=tag.getName()%>">
    </div>
    <div class="form-group">
        <label>Rank</label>
        <input type="text" name="tagRank" value="<%=tag.getRank()%>">
    </div>
    <div>
        <lable>Project</lable>
        <input type="number" name="associatedProject" value="<%=tag.getProject().getId()%>">
    </div>
    <input type="submit" value="delete">
</form>
<div><a href="../index.jsp">Home</a></div>

</body>
</html>