<%@ page import="Model.Tag" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Banger
  Date: 09.07.2020
  Time: 09:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>view Tags</title>
<body>
<table>
    <thead>
    <td>ID</td>
    <td>Name</td>
    <td>Rank</td>
    <td>update</td>
    <td>delete</td>
    <td>get Tickets</td>
    <td>modify Tags</td>
    </thead>
    <%
        Tag tag = (Tag) request.getAttribute("tag");
        List<Tag> tags = (List<Tag>) request.getAttribute("tags");
        List<Tag> tagsWhichCanBeAdded = (ArrayList<Tag>) request.getAttribute("tagsCanBeAdded");
        if (tag != null || tags != null) {
            if (tag != null && tags == null) {
                tags = new ArrayList<Tag>();
                tags.add(tag);
            }
            for (Tag t : tags) {
                int id = t.getId();
                String name = t.getName();
                int rank = t.getRank();
    %>
    <tr>
        <td><%=id%>
        </td>
        <td><%=name%>
        </td>
        <td><%=rank%>
        </td>
        <td>
            <form action="updateTagRequest" method="get">
                <input name="tagId" type="hidden" value="<%=id%>">
                <input type="submit" value="update tag">
            </form>
        </td>
        <td>
            <form action="deleteTagRequest" method="get">
                <input name="tagId" type="hidden" value="<%=id%>">
                <input type="submit" value="delete tag">
            </form>
        </td>
        <td>
            <form action="getTicketsWithTag" method="get">
                <input name="tagId" type="hidden" value="<%=id%>">
                <input type="submit" value="view tickets">
            </form>
        </td>
        <% String ticketId = request.getParameter("ticketId");
            if (ticketId != null) {%>
        <td>
            <form action="removeTagFromTicket" method="post">
                <input name="ticketId" type="hidden" value=<%=ticketId%>>
                <input name="tagId" type="hidden" value=<%=id%>>
                <input type="submit" value="remove">
            </form>
        </td>
        <%
            }%>
    </tr>
    <%
        }
        if (tagsWhichCanBeAdded != null) {
    %>
    <% for (Tag t : tagsWhichCanBeAdded) {
        int id = t.getId();
        String name = t.getName();
        int rank = t.getRank();%>
    <tr>
        <td><%=id%>
        </td>
        <td><%=name%>
        </td>
        <td><%=rank%>
        </td>
        <td>
            <form action="updateTagRequest" method="get">
                <input name="tagId" type="hidden" value="<%=id%>">
                <input type="submit" value="update">
            </form>
        </td>
        <td>
            <form action="deleteTagRequest" method="get">
                <input name="tagId" type="hidden" value="<%=id%>">
                <input type="submit" value="delete">
            </form>
        </td>
        <td>
            <form action="getTicketsWithTag" method="get">
                <input name="tagId" type="hidden" value="<%=id%>">
                <input type="submit" value="view tickets">
            </form>
        </td>
        <td>
            <form action="addTagToTicket" method="post">
                <input name="ticketId" type="hidden" value=<%=request.getParameter("ticketId")%>>
                <input name="tagId" type="hidden" value=<%=id%>>
                <input type="submit" value="add">
            </form>
        </td>
    </tr>
    <%
            }
        }
    %>
    </tbody>
</table>
<%
} else {
%>


<%}%>
<div><a href="../index.jsp">Home</a></div>
</body>
</html>
