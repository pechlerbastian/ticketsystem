<%--
  Created by IntelliJ IDEA.
  User: Banger
  Date: 13.07.2020
  Time: 16:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>create User</title>
</head>
<body>
<form action="createUser" method="POST">
    <div class="form-group">
        <label>Name</label>
        <input type="text" name="userName" value="<%=request.getParameter("newUserName")%>"/>
    </div>
    <div class="form-group">
        <label>Password</label>
        <input type="password" name="userPassword" value="enter Password"/>
    </div>
    <div>
        <label></label>
        <input type="checkbox" name="userAdmin"/>
    </div>
    <input type="submit" value="create">
</form>
<div><a href="../index.jsp">Home</a></div>

</body>
</html>
