<%@ page import="Model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>delete User</title>
</head>
<body>
<div> </div>
<form action="deleteUser" method="POST">
    <%User user = (User) request.getAttribute("user");%>
    <input type="hidden" name="userId" value="<%=user.getId()%>">
    <input type="text" name="userName" value="<%=user.getName()%>">
    <input type="checkbox" name="userAdmin" <% if (user.isAdmin()) { %> checked <% } %> disabled="disabled">
    <input type="submit" value="delete">
</form>
<div><a href="../index.jsp">Home</a></div>
</body>
</html>