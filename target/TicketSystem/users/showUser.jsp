<%@ page import="Model.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Banger
  Date: 13.07.2020
  Time: 16:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>view Users</title>
</head>
<body>

<body>
<% User user = (User) request.getAttribute("user");
    List<User> users = (List<User>) request.getAttribute("users");
    List<User> usersAssignable = (List<User>) request.getAttribute("usersAssignable");
%>
<h2>view User</h2>
<table>
    <thead>
    <td>ID</td>
    <td>Name</td>
    <td>Admin</td>
    <td>update</td>
    <td>delete</td>
    <td>update Project</td>
    </thead>
    <tbody>
    <%
        if (user != null || users != null) {
            if (user != null && users == null) {
                users = new ArrayList<User>();
                users.add(user);
            }
            for (User u : users) {
                int id = u.getId();
                String name = u.getName();
                boolean admin = u.isAdmin();
    %>
    <tr>
        <td>
            <%=id%>
        </td>
        <td>
            <%=name%>
        </td>
        <td>
            <form>
                <input type="checkbox"  <% if (admin) { %> checked <% }%>disabled="disabled">
            </form>
        </td>
        <td>
            <form action="updateUserRequest" method="get">
                <input name="userId" type="hidden" value=<%=id%>>
                <input type="submit" value="update">
            </form>
        </td>
        <td>
            <form action="deleteUserRequest" method="get">
                <input name="userId" type="hidden" value=<%=id%>>
                <input type="submit" value="delete">
            </form>
        </td>
        <% String projectId = request.getParameter("projectId");
            if (projectId != null) {%>
        <td>
            <form action="removeUserFromProject" method="post">
                <input name="userId" type="hidden" value=<%=id%>>
                <input name="projectId" type="hidden" value=<%=projectId%>>
                <input type="submit" value="remove">
            </form>
        </td>
        <% }%>
    </tr>
    <%
        }
        if (usersAssignable != null) {
            for (User u : usersAssignable) {
                int id = u.getId();
                String name = u.getName();
                boolean admin = u.isAdmin();
    %>
    <tr>
        <td>
            <%=id%>
        </td>
        <td>
            <%=name%>
        </td>
        <td>
            <form>
                <input type="checkbox"  <% if (admin) { %> checked <% }%>disabled="disabled">
            </form>
        </td>
        <td>
            <form action="updateUserRequest" method="get">
                <input name="userId" type="hidden" value=<%=id%>>
                <input type="submit" value="update">
            </form>
        </td>
        <td>
            <form action="deleteUserRequest" method="get">
                <input name="userId" type="hidden" value=<%=id%>>
                <input type="submit" value="delete">
            </form>
        </td>
        <% String projectId = request.getParameter("projectId");
            if (projectId != null) {%>
        <td>
            <form action="addUserToProject" method="post">
                <input name="userId" type="hidden" value=<%=id%>>
                <input name="projectId" type="hidden" value=<%=projectId%>>
                <input type="submit" value="add">
            </form>
        </td>
        <% }%>
    </tr>
    <% }
    }
    %>
    </tbody>
</table>
<%
} else {
%>
<%}%>
<div><a href="../index.jsp">Home</a></div>
</body>
</html>
