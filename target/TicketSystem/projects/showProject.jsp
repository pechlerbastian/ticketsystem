<%@ page import="Model.Ticket" %>
<%@ page import="static com.sun.org.apache.xml.internal.security.keys.keyresolver.KeyResolver.length" %>
<%@ page import="java.util.List" %>
<%@ page import="Model.Project" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Banger
  Date: 30.06.2020
  Time: 14:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>view Projects</title>

</head>
<body>
<% Project project = (Project) request.getAttribute("project");
    List<Project> projects = (List<Project>) request.getAttribute("projects");%>
<h2>view Project</h2>
<table>
    <thead>
    <td>ID</td>
    <td>Name</td>
    <td>update</td>
    <td>delete</td>
    <td>add Ticket</td>
    <td>show Tickets</td>
    <td>add Tag</td>
    <td>show Tags</td>
    <td>manage Users</td>
    </thead>
    <tbody>
    <%
        if (project != null || projects != null) {
            if (project != null && projects == null) {
                projects = new ArrayList<Project>();
                projects.add(project);
            }
            for (Project p : projects) {
                int id = p.getId();
                String name = p.getName();
    %>
    <tr>
        <td><%=id%>
        </td>
        <td><%=name%>
        </td>
        <td>
            <form action="updateProjectRequest" method="get">
                <input name="projectId" type="hidden" value=<%=id%>>
                <input type="submit" value="update">
            </form>
        </td>
        <td>
            <form action="deleteProjectRequest" method="get">
                <input name="projectId" type="hidden" value=<%=id%>>
                <input type="submit" value="delete">
            </form>
        </td>
        <td>
            <form action="addTicketToProject" method="get">
                <input name="projectId" type="hidden" value="<%=id%>">
                <input name="ticketName" type="text" value="enter ticket name">
                <input type="submit" value="add">
            </form>
        <td>
            <form action="getTicketsForProject" method="get">
                <input name="projectId" type="hidden" value="<%=id%>">
                <input type="submit" value="view tickets">
            </form>
        </td>
        <td>
            <form action="addTagToProject" method="get">
                <input name="projectId" type="hidden" value="<%=id%>">
                <input name="tagName" type="text" value="enter tag name">
                <input type="submit" value="add">
            </form>
        <td>
            <form action="getTagsForProject" method="get">
                <input name="projectId" type="hidden" value="<%=id%>">
                <input type="submit" value="view tags">
            </form>
        </td>
        <td>
            <form action="modifyUsersInProject" method="get">
                <input name="projectId" type="hidden" value="<%=id%>">
                <input type="submit" value="modify Users">
            </form>
    </tr>
    <%
        }

    %>
    </tbody>
</table>
<%
} else {
%>
<%}%>
<div><a href="../index.jsp">Home</a></div>

</body>
</html>
