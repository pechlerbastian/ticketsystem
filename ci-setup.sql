ALTER SEQUENCE ticket_id_seq RESTART WITH 1;
ALTER SEQUENCE project_id_seq RESTART WITH 1;
ALTER SEQUENCE tag_id_seq RESTART WITH 1;
ALTER SEQUENCE "user_id_seq" RESTART WITH 1;

INSERT INTO PROJECT(name) VALUES ('Issue Tracking System');

INSERT INTO "user"(name, password, admin) VALUES ('Admin', 'password', true);
INSERT INTO "user"(name, password, admin) VALUES ('User', 'password', false);

INSERT INTO TICKET(name, project) VALUES ('Datenbank Configuration', 1);
INSERT INTO TICKET(name, project) VALUES ('Dao Ticket', 1);
INSERT INTO TICKET(name, project) VALUES ('Model Ticket', 1);
INSERT INTO TICKET(name, project) VALUES ('ShowTicket Website', 1);
INSERT INTO TICKET(name, project) VALUES ('Create Ticket', 1);

INSERT INTO TAG(name, rank, project) VALUES ('ToDo', 1, 1);
INSERT INTO TAG(name, rank, project) VALUES ('Done', 2, 1);

INSERT INTO TICKET_TAG(ticket, tag) VALUES (1, 1);

INSERT INTO USER_PROJECT("user", project) VALUES (1,1);
INSERT INTO USER_PROJECT("user", project) VALUES (2,1);